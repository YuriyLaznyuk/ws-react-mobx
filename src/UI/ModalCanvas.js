import React, { useState } from 'react';
import '../style/modalCanvas.scss';
import canvasState from '../store/canvasState';

const ModalCanvas = () => {

  const [username, setModal] = useState('');
  const [visible, setVisible] = useState(false);

  const test = /^[A-Za-zА-Яа-я]{2,}$/g.test(username);

  const clickBtn = () => {
    setVisible(true);
    canvasState.setUsername(username);
  };

  return (
    <div className='modalCanvas' style={{ display: visible && 'none' }}>
      <div className="modalCanvas__block">
        <label htmlFor='inp' className="modalCanvas__block-title">ENTER USERNAME</label>
        <label
          className="modalCanvas__block-valid">{((!test && username.length > 0)) ? 'username at least two letters' : ''}</label>
        <div className="modalCanvas__block-elem">
          <input
            onChange={e => setModal(e.target.value)}
            value={username}
            className="my-input" id='inp' placeholder='username' type="text"/>
        </div>
        <div className="modalCanvas__block-elem">
          <button onClick={ clickBtn}
                  disabled={!test} className="my-btn">SAVE
          </button>
        </div>
      </div>

    </div>
  );
};

export default ModalCanvas;