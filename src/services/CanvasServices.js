import canvasState from '../store/canvasState';
import toolsState from '../store/toolsState';
import Brush from '../tools/Brush';
import Eraser from '../tools/Eraser';
import Rect from '../tools/Rect';
import Circle from '../tools/Circle';
import Line from '../tools/Line';

export default  class CanvasServices {

  static host = window.location.origin;
  static urlWs = window.location.origin.replace('http', 'ws');


  static fetchImg(id,ctx,canvasRef){
   fetch(this.host + `/api/img/${id}`).then(res => res.json())
     .then(json => {
       const img = new Image();
       img.src = json;
       img.onload = () => {
         ctx.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
         ctx.drawImage(img, 0, 0, canvasRef.current.width, canvasRef.current.height);
       };
     }).catch(err => console.log(err));
  }


static webSocketCanvas(id,canvasRef){
  if (canvasState.username) {
    const socket = new WebSocket(this.urlWs);
    canvasState.setSocket(socket);
    canvasState.setSessionID(id);

    toolsState.setTool(new Brush(canvasRef.current, socket, id));

    socket.onopen = () => {
      console.log('the client is connected to the socket');
      socket.send(JSON.stringify({
        id: id,
        username: canvasState.username,
        method: 'connection'
      }));
    };
    socket.onmessage = mes => {
      let msg = JSON.parse(mes.data);
      switch (msg.method) {
        case 'connection':
          console.log(`user ${msg.username} is connected`);
          break;
        case 'draw':
          this.drawHandler(msg,canvasRef);
          break;
      }
    };

  }
}


  static drawHandler (msg,canvasRef){
    const figure = msg.figure;
    const _ctx = canvasRef.current.getContext('2d');
    switch (figure.type) {

      case 'brush':
        Brush.draw(_ctx, figure.x, figure.y,
          figure.strokeStyle, figure.fillStyle, figure.lineWidth);
        break;

      case 'eraser':
        Eraser.draw(_ctx, figure.x, figure.y);
        break;

      case 'rect':
        Rect.staticDraw(_ctx, figure.x, figure.y, figure.width, figure.height,
          figure.strokeStyle, figure.fillStyle, figure.lineWidth);
        _ctx.beginPath();
        break;

      case 'circle':
        Circle.staticDraw(_ctx, figure.x, figure.y,
          figure.width, figure.strokeStyle, figure.fillStyle, figure.lineWidth);
        _ctx.beginPath();
        break;

      case 'line':
        Line.staticDraw(_ctx, figure.x, figure.y, figure.currentX, figure.currentY,
          figure.strokeStyle, figure.fillStyle, figure.lineWidth);
        _ctx.beginPath();
        break;

      case 'finish':
        _ctx.beginPath();
        break;

    }

  };


 static mouseUpHandler(canvasRef,id){
    //save picture user after mouse up in array

    canvasState.pushUndoList(canvasRef.current.toDataURL());

    fetch(this.host + '/api/img', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify({
        id: id,
        img: canvasRef.current.toDataURL()
      })
    }).then(res => res.json())
      .then(json => console.log(json.message))
      .catch(err => console.log(err));


  };


}