import { makeAutoObservable } from 'mobx';

class ToolsState {

  tool=null;
  _fillColor='#000000';
  _lineSize=1;
  _strokeColor='#000000';


  constructor(){
   makeAutoObservable(this)
  }

  setTool(toll){
    this.tool=toll;
  }




  setFillColor(color){
    this._fillColor=color;
    this.tool.fillColor=color;
  }

  setLineSize(size){
    this._lineSize=size;
    this.tool.lineSize=size;
  }

  setStrokeColor(color){
    this._strokeColor=color;
    this.tool.strokeColor=color;
  }

}

export default new ToolsState();