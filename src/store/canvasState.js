import { makeAutoObservable } from 'mobx';

class CanvasState {

  canvas = null;
  undoList = [];
  redoList = [];
  username = '';
  //--socket--//
  socket=null;
  sessionID=null;

  constructor() {
    makeAutoObservable(this);
  }

  setSocket(socket) {
    this.socket = socket;
  }

  setSessionID(sessionID) {
    this.sessionID = sessionID;
  }

  setUsername(username) {
    this.username = username;
  }

  setCanvas(canvas) {
    this.canvas = canvas;
  }

  pushUndoList(data) {
    this.undoList.push(data);
  }

  pushRedoList(data) {
    this.redoList.push(data);
  }

  undo() {
    let ctx = this.canvas.getContext('2d');
    if (this.undoList.length > 0) {
      let dataUrl = this.undoList.pop();

      //save in redoList current state canvas
      this.redoList.push(this.canvas.toDataURL());

      //create img
      let img = new Image();
      img.src = dataUrl;

      // after image load
      img.onload = () => {
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      };

    } else {
      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

  }

  redo() {

    let ctx = this.canvas.getContext('2d');
    if (this.redoList.length > 0) {

      let dataUrl = this.redoList.pop();

      //save in undoList current state canvas
      this.undoList.push(this.canvas.toDataURL());

      //create img
      let img = new Image();
      img.src = dataUrl;

      // after image load
      img.onload = () => {
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      };

    }

  }
}

export default new CanvasState();