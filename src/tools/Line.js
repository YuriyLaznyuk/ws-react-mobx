import Tool from './Tool';
import toolsState from '../store/toolsState';

export default class Line extends Tool{
  constructor(canvas, socket, id){
    super(canvas, socket, id);
    this.listen();
  }


  listen(){
    this.canvas.onmousemove=this.mouseMoveHandler.bind(this);
    this.canvas.onmousedown=this.mouseDownHandler.bind(this);
    this.canvas.onmouseup=this.mouseUpHandler.bind(this);
  }

  mouseUpHandler(){
    this.mouseDown=false;


    this.socket.send(JSON.stringify({
      method: 'draw',
      id: this.id,
      figure: {
        type: 'line',
        x: this.finishX,
        y: this.finishY,
        currentX:this.currentX,
        currentY:this.currentY,
        strokeStyle:toolsState._strokeColor,
        fillStyle:toolsState._fillColor,
        lineWidth:toolsState._lineSize

      }
    }));

  }

  mouseDownHandler(e){
    this.mouseDown=true;
    this.currentX=e.pageX-e.target.offsetLeft;
    this.currentY=e.pageY-e.target.offsetTop;
    this.saved = this.canvas.toDataURL();
  }



  mouseMoveHandler(e){
    if (this.mouseDown){

      this.finishX=e.pageX-e.target.offsetLeft;
      this.finishY=e.pageY-e.target.offsetTop;

      // this.draw(e.pageX-e.target.offsetLeft, e.pageY-e.target.offsetTop);
      this.draw(this.finishX, this.finishY);
    }
  }

  draw(x,y){
    const img=new Image();
    img.src=this.saved;
    img.onload=()=>{
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.drawImage(img,0, 0, this.canvas.width, this.canvas.height);
      this.ctx.beginPath();
      this.ctx.moveTo(this.currentX,this.currentY);
      this.ctx.lineTo(x,y);
      this.ctx.stroke();
    }

  }

  static staticDraw(ctx,finishX,finishY, currentX,currentY
    , strokeStyle, fillStyle, lineWidth){
    ctx.beginPath();
    ctx.strokeStyle=strokeStyle;
    ctx.fillStyle=fillStyle;
    ctx.lineWidth=lineWidth;
    ctx.moveTo(currentX,currentY);
    ctx.lineTo(finishX,finishY);
    ctx.stroke();
  }


}