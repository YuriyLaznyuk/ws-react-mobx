import Tool from './Tool';
import toolsState from '../store/toolsState';

export default class Circle extends Tool {
  constructor(canvas,socket,id) {
    super(canvas,socket,id);
    this.listen();
  }

  listen() {
    this.canvas.onmousemove = this.mouseMoveHandler.bind(this);
    this.canvas.onmousedown = this.mouseDownHandler.bind(this);
    this.canvas.onmouseup = this.mouseUpHandler.bind(this);
  }

  mouseUpHandler() {
    this.mouseDown = false;

    this.socket.send(JSON.stringify({
      method: 'draw',
      id: this.id,
      figure: {
        type: 'circle',
        x: this.startX,
        y: this.startY,
        width: this.width,
        strokeStyle:toolsState._strokeColor,
        fillStyle:toolsState._fillColor,
        lineWidth:toolsState._lineSize
      }
    }));

  }

  mouseDownHandler(e) {
    this.mouseDown = true;
    this.ctx.beginPath();
    this.startX = e.pageX - e.target.offsetLeft;
    this.startY = e.pageY - e.target.offsetTop;
    //save picture
    this.saved = this.canvas.toDataURL();

  }

  mouseMoveHandler(e) {
    if (this.mouseDown) {
      let currentX = e.pageX - e.target.offsetLeft;
      this.width = currentX - this.startX;
      this.draw(this.startX, this.startY, this.width);
    }
  }

  draw(x, y, w) {

    const img = new Image();
    img.src = this.saved;
    img.onload = () => {
      this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      this.ctx.drawImage(img,0, 0, this.canvas.width, this.canvas.height);
      this.ctx.beginPath();
      this.ctx.arc(x, y, w, 0, 2*Math.PI);
      this.ctx.fill();
      this.ctx.stroke();
    };

  }

  static staticDraw(ctx,x,y,w,strokeStyle,fillStyle,lineWidth){


    ctx.beginPath();
    ctx.strokeStyle=strokeStyle;
    ctx.fillStyle=fillStyle;
    ctx.lineWidth=lineWidth;
    ctx.arc(x, y, w, 0, 2*Math.PI);
    ctx.fill();
    ctx.stroke();

  }

}