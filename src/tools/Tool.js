import toolsState from '../store/toolsState';

export default class Tool {
  constructor(canvas, socket, id) {
    this.canvas = canvas;
    this.socket = socket;
    this.id = id;
    this.ctx = canvas.getContext('2d');
    this.destroyEvents();
  }

  destroyEvents() {
    this.canvas.onmousemove = null;
    this.canvas.onmousedown = null;
    this.canvas.onmouseup = null;

    this.ctx.strokeStyle = toolsState._strokeColor;
    this.ctx.fillStyle = toolsState._fillColor;
    this.ctx.lineWidth = toolsState._lineSize;

  }

  set fillColor(color) {
    this.ctx.fillStyle = color;
  }

  set lineSize(width) {
    this.ctx.lineWidth = width;
  }

  set strokeColor(color) {
    this.ctx.strokeStyle = color;
  }
}

