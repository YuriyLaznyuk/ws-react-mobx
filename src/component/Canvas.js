import React, { useEffect, useRef } from 'react';
import '../style/canvas.scss';
import { observer } from 'mobx-react-lite';
import canvasState from '../store/canvasState';
import ModalCanvas from '../UI/ModalCanvas';
import { useParams } from 'react-router-dom';
import CanvasServices from '../services/CanvasServices';

const Canvas = observer(() => {
  const canvasRef = useRef();
  const canvas = canvasRef.current;
  const { id } = useParams();
  const ctx = canvas?.getContext('2d');


  useEffect(() => {
    canvasState.setCanvas(canvasRef.current);
    let ctx = canvasRef.current.getContext('2d');
    CanvasServices.fetchImg(id,ctx,canvasRef);
  }, []);

  //connect WS
  useEffect(() => {
CanvasServices.webSocketCanvas(id,canvasRef)

  }, [canvasState.username]);


  return (
    <div className='canvas'>
      <ModalCanvas/>
      <canvas onMouseUp={() => CanvasServices.mouseUpHandler(canvasRef,id)}
              ref={canvasRef} width={800} height={600}/>

      <button onClick={() => {
        ctx && ctx.clearRect(0, 0, canvas.width, canvas.height);
      }}>Clear Canvas
      </button>

    </div>
  );
});

export default Canvas;