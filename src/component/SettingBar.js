import React from 'react';
import '../style/toolBar.scss';
import toolsState from '../store/toolsState';

const SettingBar = () => {
  return (
    <div className='settingBar'>
       <span className='toolBar__left-btn'>line thickness &nbsp;
         <input style={{ padding: 0, cursor: 'pointer', width: 50, fontSize: 18 }}
         onChange={e=>toolsState.setLineSize(e.target.value)}
          type='number' defaultValue={1} min={1} max={20} title='line thickness'/></span>


      <span className='toolBar__left-btn'>stroke color &nbsp;
        <input style={{ padding: 0, cursor: 'pointer', width: 50}}
               onChange={e=>toolsState.setStrokeColor(e.target.value)}
               type='color'  title='stroke color'/></span>

    </div>
  );
};

export default SettingBar;