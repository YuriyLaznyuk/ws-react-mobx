import React from 'react';
import '../style/toolBar.scss';
import toolsState from '../store/toolsState';
import Brush from '../tools/Brush';
import Rect from '../tools/Rect';
import Circle from '../tools/Circle';
import canvasState from '../store/canvasState';
import Eraser from '../tools/Eraser';
import Line from '../tools/Line';

const ToolBar = () => {

  const selectColor = e => {
    toolsState.setFillColor(e.target.value);
    // toolsState.setStrokeColor(e.target.value);
  };

  const download = () => {
    //current image from canvas
    const dataUrl = canvasState.canvas.toDataURL();
    console.log(dataUrl);
    const link = document.createElement('a');
    link.href = dataUrl;
    link.download = canvasState.sessionID + '.jpg';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <div className='toolBar'>
      <div className="toolBar__left">
        <span className='toolBar__left-btn' onClick={() =>
          toolsState.setTool(new Brush(canvasState.canvas, canvasState.socket, canvasState.sessionID))}>
          <img src={require('../img/brush.png')} alt=""/></span>
        <span className='toolBar__left-btn' onClick={() =>
          toolsState.setTool(new Rect(canvasState.canvas, canvasState.socket, canvasState.sessionID))}>
          <img src={require('../img/rect.png')} alt=""/></span>
        <span className='toolBar__left-btn' onClick={() =>
          toolsState.setTool(new Circle(canvasState.canvas, canvasState.socket, canvasState.sessionID))}>
          <img src={require('../img/circle.png')} alt=""/></span>
        <span className='toolBar__left-btn' onClick={() =>
          toolsState.setTool(new Eraser(canvasState.canvas, canvasState.socket, canvasState.sessionID))}>
          <img src={require('../img/eraser.png')} alt=""/></span>
        <span className='toolBar__left-btn' onClick={() =>
          toolsState.setTool(new Line(canvasState.canvas, canvasState.socket, canvasState.sessionID))}>
          <img src={require('../img/line.png')} alt=""/></span>
        <span onChange={e => selectColor(e)}
              className='toolBar__left-btn'>select color &nbsp;<input style={{ padding: 0, cursor: 'pointer' }}
                                                                      title='select color' type='color'/></span>


      </div>
      <div className="toolBar__right">
        <span onClick={() => canvasState.undo()} className='toolBar__right-btn' title='undo'>
          <img src={require('../img/undo.png')} alt=""/></span>
        <span className='toolBar__right-btn' onClick={() => canvasState.redo()} title='redo'>
          <img src={require('../img/redo.png')} alt=""/></span>
        <span onClick={()=>download()} className='toolBar__right-btn' title='save'>
          <img src={require('../img/save.png')} alt=""/></span>

      </div>
    </div>
  );
};

export default ToolBar;