import React from 'react';
import './style/app.scss';
import ToolBar from './component/ToolBar';
import SettingBar from './component/SettingBar';
import Canvas from './component/Canvas';
import { BrowserRouter ,Switch, Route, Redirect} from 'react-router-dom';

const App = () => {
  return (
    <BrowserRouter>
    <div className='app'>

      <Switch>
        <Route exect path='/:id'>
          <ToolBar/>
          <SettingBar/>
          <Canvas/>
        </Route>
        <Redirect to={`f${(+new Date).toString(16)}`}/>

      </Switch>
    </div>

    </BrowserRouter>
  );
};

export default App;