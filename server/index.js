const express = require('express');
const app = express();
const expressWs = require('express-ws')(app);
const sWss = expressWs.getWss();
require('dotenv').config({ path: './public/.env' });
const { handlers } = require('./handlers/connectionHandler');
const path = require('path');
const cors = require('cors');
const mongoose = require('mongoose');
const apiRouter = require('./routes/api-routes');
const PORT = process.env.PORT || 3443;

app.use(cors());
app.use(express.json());
app.use(express.static(path.resolve('dist')));

const DB = process.env.DATABASE.replace('<password>', process.env.PASSWORD_DB);
const reg = /^(\/|\/[0-9a-f]{9,12})$/;

app.get(reg, (req, res) => {
  res.status(200).sendFile(path.resolve('dist', 'index.html'));
});

app.ws('/', (ws, req) => {
  console.log('ws connect port ', PORT);
  ws.on('message', ms => {
    ms = JSON.parse(ms);
    switch (ms.method) {
      case 'connection':
        handlers.connectHandler(ws, ms, sWss);
        break;

      case 'draw':
        handlers.broadcastHandler(ws, ms, sWss);
        break;
    }
  });

});

app.use(apiRouter);

mongoose.connect(DB,
  { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('DB connection'))
  .catch(err => console.log('ERROR  ', err.message));

app.listen(PORT, () => {
  console.log(`Server port: ${PORT} `);
});
