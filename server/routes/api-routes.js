const express = require('express');
const router = express.Router();
const apiController=require('../controllers/api-controller')

router
  .route('/api/img')
  .post(apiController.postImgCanvas);

router
  .route('/api/img/:id')
  .get(apiController.getIdImgCanvas);


module.exports=router;
