const path = require('path');
const fs = require('fs');
const CanvasFile = require('../models/canvasFile');

const postImgCanvas = async (req, res) => {
  try {

    const data = req.body.img.replace('data:image/png;base64,', '');
    // fs.writeFileSync(path.resolve('dist', 'files', `${req.body.id}.jpg`), data, 'base64');

    const isExistId = await CanvasFile.exists({ id: req.body.id });
    if (!isExistId) {
      await CanvasFile.create({ id: req.body.id, name: `${req.body.id}.jpg`, file: data });

    } else {

      await CanvasFile.updateOne({ id: req.body.id }, { file: data });
    }

    return res.status(200).json({ message: 'file is loaded on mongoDB' });

  } catch (e) {
    console.log(e);
    return res.status(500).json({ message: 'error /api/img ' + e.message });
  }
};

const getIdImgCanvas = async (req, res) => {
  try {
    // const imgPath = path.resolve('dist', 'files', `${req.params.id}.jpg`);
    // const file = fs.readFileSync(imgPath);
    // const data = 'data:image/png;base64,' + file.toString('base64');

    const isExistId = await CanvasFile.exists({ id: req.params.id });

    if (!isExistId) {
      return res.json({ message: 'session file does not exist in mongoDB' });
    } else {

      const _data = await CanvasFile.findOne({ id: req.params.id });
      const _file = _data.file;
      const data = 'data:image/png;base64,' + _file.toString('base64');
      return res.json(data);

    }

  } catch (e) {
    console.log(e);
    return res.status(500).json({ message: 'error /api/img ' + e.message });
  }
};

module.exports = { postImgCanvas, getIdImgCanvas };