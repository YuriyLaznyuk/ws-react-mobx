


const handlers= {

  //--method connection--//
  connectHandler(ws, msg, sWss) {
    //id session
    ws.id = msg.id;
    this.broadcastHandler(ws,msg,sWss)
  },
//broadcast for all connections
  broadcastHandler(ws, msg, sWss){
    sWss.clients.forEach(client=>{
      if(client.id===msg.id){
        client.send(JSON.stringify(msg));
      }
    });

  },

//--method draw--//
  broadcastConnection(ws, msg, sWss){
    sWss.clients.forEach(client=>{
      if(client.id===msg.id){
        client.send(JSON.stringify(msg));
      }
    });

  },

};

module.exports={handlers};



