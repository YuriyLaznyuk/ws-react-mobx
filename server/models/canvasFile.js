const mongoose = require('mongoose');
const canvasFileSchema = new mongoose.Schema({
  id: {
    type: String,
    unique: true
  },
name:{
    type:String,
},
  file: {
    type: String

  }
});

const CanvasFile=mongoose.model('CanvasFile',canvasFileSchema);
module.exports=CanvasFile;